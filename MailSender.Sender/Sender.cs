﻿using FluentMailer.Factory;
using FluentMailer.Interfaces;
using MailSender.DataTypes;
using MailSender.Logger;
using System.Collections.Generic;

namespace MailSender.Sender
{
    public class Sender : ISender
    {
        readonly IFluentMailer _FluentMailer;
        readonly ILogger _Logger;

        public Sender(ILogger Logger)
        {
            _Logger = Logger;
            _FluentMailer = FluentMailerFactory.Create();
        }

        public void SendMails(List<Person> data, ProgramInfo programInfo)
        {
            data.ForEach( x =>
            {
                _Logger.AddLogEntry($"Rozpoczynam wysywłanie maila: {x.Name}");
                _FluentMailer.CreateMessage()
                         .WithView(programInfo.MailBodyTemplateName, x)
                        .WithReceiver(x.Email)
                        .WithSubject("Zostań konsultantem AVON!!")
                        .Send();
            });
        }
    }
}
