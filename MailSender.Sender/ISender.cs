﻿using System.Collections.Generic;
using MailSender.DataTypes;

namespace MailSender.Sender
{
    public interface ISender
    {
        void SendMails(List<Person> data, ProgramInfo programInfo);
    }
}