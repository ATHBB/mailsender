﻿using CsvHelper;
using CsvHelper.Configuration;
using MailSender.DataTypes;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Serialization;

namespace MailSender.FileReader
{
    public class FileReader : IFileReader
    {
        public ProgramInfo Deserialize(string filePath)
        {
            if (File.Exists(filePath))
            {
                var deserializer = new XmlSerializer(typeof(ProgramInfo));
                using (var reader = new StreamReader(filePath))
                    return (ProgramInfo)deserializer.Deserialize(reader);
            }
            return new ProgramInfo { DataFilePath = ConfigFileDefaults._dataFilePath, Iteration = ConfigFileDefaults._iteration, MailBodyTemplateName = ConfigFileDefaults._mailBodyTemplateName };
        }

        public List<Person> GetDatFromFileFile(string filePath, int iteration)
        {
            if (File.Exists(filePath))
            {
                using (var sw = new StreamReader(filePath))
                {
                    var csv = new CsvReader(sw, new CsvConfiguration
                    {
                        HasHeaderRecord = false,
                        Delimiter = ";"
                    });
                    return csv.GetRecords<Person>().Skip(iteration*100).Take(100).ToList();
                }
            }
            return new List<Person>();
        }

        public void Serialize(string filePath, ProgramInfo objectToSerialize)
        {
            objectToSerialize.Iteration++;
            var serializer = new XmlSerializer(typeof(ProgramInfo));
            using (var sw = new StreamWriter(filePath))
                serializer.Serialize(sw, objectToSerialize);
        }
    }
}
