﻿namespace MailSender.FileReader
{
    public static class ConfigFileDefaults
    {
        public const int _iteration = 0;
        public const string _mailBodyTemplateName = @"MailTemplate.cshtml";
        public const string _dataFilePath = @"B:\Studia\Mgr\Sem 1\Zaawansowane techniki programowania\mail.csv";
        public const string _programConfigFilePath = @"B:\Studia\Mgr\Sem 1\Zaawansowane techniki programowania\MailSender\MailSender\bin\Debug\ProgramConfig.xml";
    }
}
