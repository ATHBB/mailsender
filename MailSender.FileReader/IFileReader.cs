﻿using System.Collections.Generic;
using MailSender.DataTypes;

namespace MailSender.FileReader
{
    public interface IFileReader
    {
        ProgramInfo Deserialize(string filePath);
        List<Person> GetDatFromFileFile(string filePath, int iteration);
        void Serialize(string filePath, ProgramInfo objectToSerialize);
    }
}