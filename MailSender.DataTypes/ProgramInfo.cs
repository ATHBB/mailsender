﻿namespace MailSender.DataTypes
{
    public class ProgramInfo
    {
        public int Iteration { get; set; }
        public string MailBodyTemplateName { get; set; }
        public string DataFilePath { get; set; }
    }
}
