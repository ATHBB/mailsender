﻿namespace MailSender.DataTypes
{
    public class Person
    {
        public string Name { get; set; }

        public string SecondName { get; set; }

        public string Email { get; set; }
    }
}
