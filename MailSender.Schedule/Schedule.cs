﻿using MailSender.DataTypes;
using MailSender.FileReader;
using MailSender.Logger;
using MailSender.Sender;
using Quartz;
using Quartz.Impl;
using System;

namespace MailSender.Schedule
{
    public class Schedule : IJob
    {
        readonly ILogger _Logger;
        IFileReader _FileReader;
        ISender _Sender;
        
    
        public Schedule()
        {
            _Logger = new Logger.Logger();
            _FileReader = new FileReader.FileReader();
            _Sender = new Sender.Sender(_Logger);
        }

        public void CreateSchedule()
        {
            try
            {
                var scheduler = StdSchedulerFactory.GetDefaultScheduler();
                var job = JobBuilder.Create<Schedule>().Build();
                var trigger = TriggerBuilder.Create().
                    StartNow().
                    WithSimpleSchedule(x => x.WithIntervalInSeconds(60).
                    RepeatForever()).
                    Build();
                StartSchedule(scheduler, job, trigger);
                _Logger.AddLogEntry("Utworzono schedule");
            }
            catch (SchedulerException ex)
            {
                _Logger.AddLogError("CreateSchedule", ex);
            }
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var program = _FileReader.Deserialize(ConfigFileDefaults._programConfigFilePath);
                _FileReader.Serialize(ConfigFileDefaults._programConfigFilePath, program);
                _Sender.SendMails(_FileReader.GetDatFromFileFile(program.DataFilePath, program.Iteration), program);
                _Logger.AddLogEntry($"Wykonano iterację: {program.Iteration}");
            }
            catch (Exception ex)
            {
                _Logger.AddLogError("Execute", ex);
            }
        }

        public void Start()
        {
            _Logger.InitializeLogger();
            CreateSchedule();
        }

        public void Stop()
        {

        }

        void StartSchedule(IScheduler scheduler, IJobDetail job, ITrigger trigger)
        {
            try
            {
                scheduler.Start();
                scheduler.ScheduleJob(job, trigger);
            }
            catch (SchedulerException ex)
            {
                _Logger.AddLogError("StartSchedule", ex);
            }
        }

        public void StopSchedule(IScheduler scheduler)
        {
            try
            {
                scheduler.Shutdown();
            }
            catch (SchedulerException ex)
            {
                _Logger.AddLogError("StopSchedule", ex);
            }
        }
    }
}
