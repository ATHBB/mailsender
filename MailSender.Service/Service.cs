﻿using Topshelf;

namespace MailSender.Service
{
    public class Service 
    {
        public void InitializeService()
        {
            HostFactory.Run(configure =>
            {
                configure.Service<Schedule.Schedule> (service =>
                {
                    service.ConstructUsing(() => new Schedule.Schedule());
                    service.WhenStarted(s => s.Start());
                    service.WhenStopped(s => s.Stop());
                });
                configure.RunAsLocalSystem();
                configure.SetServiceName("MailSender");
                configure.SetDisplayName("MailSender");
                configure.SetDescription("Usługa na zajecia");
            });
        }
    }
}
